<?php
/**
 * Counter.php
 *
 * @package    Reservations
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Counter extends Eloquent
{
    protected $fillable = ['_id', 'seq'];
    protected $collection = 'counters';

    public static function getNextSequence($name)
    {
        $counter = Counter::find($name);
        if (!$counter) {
            $counter = new Counter();
            $counter->_id = $name;
            $counter->seq = 1;
            $counter->save();
            return 1;
        }
        $counter->increment('seq');
        return $counter->seq;
    }
}