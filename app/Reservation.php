<?php
/**
 * Reservation.php
 *
 * @package    Reservations
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Reservation extends Eloquent
{
    protected $fillable = [
        '_id',
        'date_start',
        'date_end',
        'title',
        'description',
        'recur_id',
        'strict'
    ];

    protected $collection = 'reservations';
}