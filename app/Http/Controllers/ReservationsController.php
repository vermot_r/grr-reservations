<?php
/**
 * ReservationsController.php
 *
 * @package    Reservations
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace App\Http\Controllers;

use App\Article;
use App\Counter;
use App\Reservation;
use Illuminate\Http\Request;

class ReservationsController extends Controller
{
    public function create(Request $request)
    {
        $inputs = $request->only('date_start',
            'date_end',
            'title',
            'description',
            'recur_id',
            'strict');

        $validator = \Validator::make($inputs, [
            'date_start' => 'required|date',
            'date_end' => 'required|date',
            'title' => 'required',
            'description' => 'required',
            'recur_id' => 'required|numeric',
            'strict' => 'required|boolean'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages() ], 400);
        } else {

            $inputs['_id'] = Counter::getNextSequence('reservations');
            $inputs['date_start'] = new \MongoDate(strtotime($inputs['date_end']));
            $inputs['date_end'] = new \MongoDate(strtotime($inputs['date_end']));

            Reservation::create($inputs);
            return response()->json(['id' => $inputs['_id']]);
        }
    }
}