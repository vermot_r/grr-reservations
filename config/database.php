<?php
/**
 * database.php
 *
 * @package    Reservations
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

return [

    'default' => 'mongodb',

    'connections' => [

        'mongodb' => [
            'driver'    => 'mongodb',
            'host'      => env('DB_HOST', 'localhost'),
            'port'      => env('DB_PORT', 27017),
            'username'  => env('DB_USERNAME', ''),
            'password'  => env('DB_PASSWORD', ''),
            'database'  => env('DB_DATABASE', ''),
        ],

    ],

    'migrations' => 'migrations',
];